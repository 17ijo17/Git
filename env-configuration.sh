if [ -z "$2" ]; then
	echo "Please provide e-mail adress and your name e.g.:\n"
	echo "   $ ./env-configuration.sh jan.kowalski@gmail.com Jan Kowalski"
	echo "   or"
	echo "   $ ./env-configuration.sh jan.kowalski@gmail.com kowal"
else 
	echo "Configuring git..."
	git config --global user.name "$2 $3"
	git config --global user.email $1

	ssh-keygen -t rsa -C $1

	echo -e "Adding merge tool and diff tool..."
	git config --global merge.tool kdiff3
	git config --global mergetool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
	git config --global mergetool.kdiff3.trustExitCode false
	git config --global diff.guitool kdiff3
	git config --global difftool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
	git config --global difftool.kdiff3.trustExitCode false
fi
